FROM akshshar/restztp-base:latest 

ADD ./version.json version.json
COPY ./lib/ /usr/local/lib/xr-restztp/
EXPOSE 5000

CMD ["/usr/bin/python3", "/usr/local/lib/xr-restztp/restful_ztp_hook.py"]
