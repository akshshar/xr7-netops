# xr7-netops

Sample code from Devnet Day2020 demo to showcase XR7 netops capabilities from ZTP with Cisco crosswork, XR DNF based install, streaming telemetry, YDK based configuration to the deployment of Facebook's Open/R on XR using Gitlab CI/CD workflows.

## Understanding the pieces

<img src="./images/xr7-netops-gitlab-ci-cd-flow.png" alt="netops flow" width="80%" align="middle">  

&nbsp;  
&nbsp;  



### Basic Pipeline Flow
* The entire automation workflow spans ZTP, XR install, Telemetry, YDK and Open/R bring-up (which uses IOS-XR Service Layer APIs).
* This automation workflow leverages 5 GitLab CI/CD pipelines with the following trigger order:


<img src="./images/xr7-netops-pipeline-flow.png" alt="pipeline flow" width="100%" align="middle">



## Dive Deeper
To learn more, watch the Service-Provider Track at Cisco Live DevNet Day 2020: https://developer.cisco.com/events/devnetday20/ on June 18, 2020.
